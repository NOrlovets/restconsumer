package com.epam.demo.restconsumer.controllers;

import com.epam.demo.restconsumer.domain.Product;
import com.epam.demo.restconsumer.service.CarsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Timur_Khudiakov on 9/5/2017.
 */
@RestController
public class CarsController {
    Logger logger = LoggerFactory.getLogger(CarsController.class);

    @Autowired
    private CarsService carsService;

    @GetMapping("/cars")
    public List<Product> getCarsByBrand() {
        for (Product product: carsService.getCarsByBrand()){
            logger.info(product.toString());
        }
        return carsService.getCarsByBrand();
    }

}
