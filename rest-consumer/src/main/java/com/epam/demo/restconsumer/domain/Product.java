package com.epam.demo.restconsumer.domain;

/**
 * Created by Timur_Khudiakov on 9/5/2017.
 */
public class Product {

    private int id;

    private String name;

    private int price;

    private int amount;

    private int idBrand;

    private int idCategory;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                ", idBrand=" + idBrand +
                ", idCategory=" + idCategory +
                '}';
    }



//    @ManyToMany(mappedBy = "products")
//    private List<Cart> carts = new ArrayList<>();

    public Product() {
    }

    public Product(String name, int price, int amount, int idBrand, int idCategory) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.idBrand = idBrand;
        this.idCategory = idCategory;
    }

//    public List<Cart> getCarts() {
//        return carts;
//    }
//
//    public void setCarts(Cart cart) {
//        this.carts.add(cart);
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdBrand() {
        return idBrand;
    }

    public void setIdBrand(int idBrand) {
        this.idBrand = idBrand;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

}
